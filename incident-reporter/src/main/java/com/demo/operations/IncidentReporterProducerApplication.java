package com.demo.operations;

import com.demo.operations.generator.IncidentEventGenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.OrderingStrategy;
import io.axual.client.config.SpecificAvroProducerConfig;
import io.axual.client.producer.ProduceCallback;
import io.axual.client.producer.ProducedMessage;
import io.axual.client.producer.Producer;
import io.axual.client.producer.ProducerMessage;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;

@Component
public class IncidentReporterProducerApplication {
    private final static Logger LOG = LoggerFactory.getLogger(IncidentReporterProducerApplication.class.getName());

    private Producer<Application, ApplicationIncidentEvent> producer;
    private IncidentEventGenerator incidentEventGenerator;

    private final static String STREAM = "application-incidents";

    public IncidentReporterProducerApplication(@Autowired IncidentEventGenerator eventGenerator) {
        final AxualClient axualClient = new AxualClient(getClientConfig());

        // Both the AxualClient and the Producer are AutoCloseable, so can be used in a try-with-resources block.
        producer = axualClient.buildProducer(getAvroProducerConfig());
        incidentEventGenerator = eventGenerator;
    }

    @Scheduled(fixedRate = 500)
    public void produce() {
        ApplicationIncidentEvent applicationIncidentEvent = incidentEventGenerator.generate();
        Application application = incidentEventGenerator.getKey(applicationIncidentEvent);

        ProducerMessage<Application, ApplicationIncidentEvent> accountEntryMessage = ProducerMessage.<Application, ApplicationIncidentEvent>newBuilder()
                .setStream(STREAM)
                .setKey(application)
                .setValue(applicationIncidentEvent)
                .build();

        try {
            producer.produce(accountEntryMessage, new ProduceCallback<Application, ApplicationIncidentEvent>() {
                @Override
                public void onComplete(ProducedMessage producedMessage) {
                    LOG.info(">>>> Produced a message on stream {} and partition {} with offset {}", producedMessage.getStream(),
                            producedMessage.getPartition(),
                            producedMessage.getOffset());
                }

                @Override
                public void onError(ProducerMessage producerMessage, ExecutionException e) {
                    LOG.warn("Something went wrong while trying to produce the message");

                }
            }).get();
        } catch (InterruptedException | ExecutionException e) {
            LOG.error("Error producing ", e);
        }
    }

    private static SpecificAvroProducerConfig<Application, ApplicationIncidentEvent> getAvroProducerConfig() {
        return SpecificAvroProducerConfig.<Application, ApplicationIncidentEvent>builder()
                .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                .setOrderingStrategy(OrderingStrategy.KEEPING_ORDER)
                .build();
    }

    private static ClientConfig getClientConfig() {
        return ClientConfig.newBuilder()
                // This is the app ID as it is configured in the self service
                .setApplicationId("com.demo.operations.incidentreporter")
                .setApplicationVersion("1.0.0")
                // The endoint of the Discovery API (used to retrieve information about bootstrap servers, schema registry, TTL etc...)
                .setEndpoint("https://platform.local:29000")
                // The environment corresponding to the shortname of the env in the self service
                .setEnvironment("dev")
                // The tenant you are part of
                .setTenant("axual")
                // The ssl configuration for your application. The certificate used should have a DN
                // matching the DN that was configured in self service
                .setSslConfig(SslConfig.newBuilder()
                        .setKeyPassword(new PasswordConfig("notsecret"))
                        .setKeystorePassword(new PasswordConfig("notsecret"))
                        .setTruststorePassword(new PasswordConfig("notsecret"))
                        .setKeystoreLocation("/Users/jumanne/Projects/axual/local/local-config/security/applications/example-producer/jks/example-producer.client.jks")
                        .setTruststoreLocation("/Users/jumanne/Projects/axual/local/local-config/security/applications/common-truststore/jks/common-truststore.jks")
                        .setEnableHostnameVerification(false)
                        .build())
                .build();
    }
}
