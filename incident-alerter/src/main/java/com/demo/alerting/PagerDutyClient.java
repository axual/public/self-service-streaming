package com.demo.alerting;

import com.demo.operations.ApplicationIncidentEvent;
import com.github.dikhan.pagerduty.client.events.PagerDutyEventsClient;
import com.github.dikhan.pagerduty.client.events.domain.EventResult;
import com.github.dikhan.pagerduty.client.events.domain.Payload;
import com.github.dikhan.pagerduty.client.events.domain.Severity;
import com.github.dikhan.pagerduty.client.events.domain.TriggerIncident;
import com.github.dikhan.pagerduty.client.events.exceptions.NotifyEventException;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;

public class PagerDutyClient {
    private static final PagerDutyEventsClient pagerDutyEventsClient = PagerDutyEventsClient.create();

    public static String fireEvent(ApplicationIncidentEvent msg) throws NotifyEventException {
        Payload payload = Payload.Builder.newBuilder()
                .setSummary(String.valueOf(msg.getMessage()))
                .setSource(String.valueOf(msg.getApplication().getName()))
                .setSeverity(Severity.valueOf(msg.getSeverity().toString()))
                .setTimestamp(OffsetDateTime.from(OffsetDateTime.ofInstant(Instant.ofEpochMilli(msg.getTimestamp()), ZoneId.systemDefault())))
                .build();

        TriggerIncident incident = TriggerIncident.TriggerIncidentBuilder
                .newBuilder("e931d08d0945459e966000ab77c42c9b", payload)
                .build();

        EventResult eventResult = pagerDutyEventsClient.trigger(incident);
        return eventResult.getDedupKey();
    }
}
