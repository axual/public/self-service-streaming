package com.demo.operations;

import com.demo.alerting.PagerDutyClient;
import com.github.dikhan.pagerduty.client.events.exceptions.NotifyEventException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.axual.client.AxualClient;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.consumer.Consumer;
import io.axual.client.consumer.ConsumerMessage;
import io.axual.client.consumer.Processor;

public class IncidentEventAlerterConsumer implements Processor<Application, ApplicationIncidentEvent>, AutoCloseable {
    private static final Logger LOG = LoggerFactory.getLogger(IncidentEventAlerterConsumer.class);

    private final Consumer<Application, ApplicationIncidentEvent> consumer;

    IncidentEventAlerterConsumer(
            final AxualClient axualClient
            , final SpecificAvroConsumerConfig<Application
                        , ApplicationIncidentEvent> consumerConfig) {
        this.consumer = axualClient.buildConsumer(consumerConfig, this);
        this.consumer.startConsuming();
    }

    @Override
    public void processMessage(ConsumerMessage<Application, ApplicationIncidentEvent> msg) {
        LOG.info("Received message on topic {} partition {} offset {} key {} value {}", msg.getSystem(), msg.getPartition(), msg.getOffset(), msg.getKey(), msg.getValue());

        // For critical messages, callout to pagerduty
        if (msg.getValue().getSeverity().equals(Severity.CRITICAL)) {
            try {
                String deDupKey = PagerDutyClient.fireEvent(msg.getValue());
                LOG.info("Successfully notified PagerDuty, dedupkey={}",deDupKey);
            } catch (NotifyEventException e) {
                LOG.error("Error notifying through PagerDuty",e);
            }
        }
    }

    @Override
    public void close() {
        this.consumer.stopConsuming();
    }

    boolean isConsuming() {
        return this.consumer.isConsuming();
    }

}
