package com.demo.operations;

import io.axual.client.AxualClient;
import io.axual.client.config.DeliveryStrategy;
import io.axual.client.config.SpecificAvroConsumerConfig;
import io.axual.client.proxy.generic.registry.ProxyChain;
import io.axual.common.config.ClientConfig;
import io.axual.common.config.PasswordConfig;
import io.axual.common.config.SslConfig;

import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.HEADER_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.LINEAGE_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.RESOLVING_PROXY_ID;
import static io.axual.client.proxy.generic.registry.ProxyTypeRegistry.SWITCHING_PROXY_ID;
import static java.lang.Thread.sleep;

public class IncidentAlerterApp {

    public static void main(String[] args) throws InterruptedException {
        ClientConfig config = ClientConfig.newBuilder()
                .setApplicationId("com.demo.operations.incidentalerter")
                .setApplicationVersion("1.0.0")
                .setEndpoint("https://platform.local:29000")
                .setTenant("axual")
                .setEnvironment("dev")
                .setSslConfig(SslConfig.newBuilder()
                        .setKeyPassword(new PasswordConfig("notsecret"))
                        .setKeystorePassword(new PasswordConfig("notsecret"))
                        .setTruststorePassword(new PasswordConfig("notsecret"))
                        .setKeystoreLocation("/Users/jumanne/Projects/axual/local/local-config/security/applications/example-consumer/jks/example-consumer.client.jks")
                        .setTruststoreLocation("/Users/jumanne/Projects/axual/local/local-config/security/applications/common-truststore/jks/common-truststore.jks")
                        .setEnableHostnameVerification(false)
                        .build())
                .build();

        SpecificAvroConsumerConfig<Application, ApplicationIncidentEvent> specificAvroConsumerConfig =
                SpecificAvroConsumerConfig.<Application, ApplicationIncidentEvent>builder()
                        .setDeliveryStrategy(DeliveryStrategy.AT_LEAST_ONCE)
                        .setStream("application-incidents")
                        .setProxyChain(ProxyChain.newBuilder()
                                .append(SWITCHING_PROXY_ID)
                                .append(RESOLVING_PROXY_ID)
                                .append(LINEAGE_PROXY_ID)
                                .append(HEADER_PROXY_ID)
                                .build())
                        .build();

        try (final AxualClient axualClient = new AxualClient(config);
             final IncidentEventAlerterConsumer consumer = new IncidentEventAlerterConsumer(axualClient, specificAvroConsumerConfig)) {
            while (consumer.isConsuming()) {
                sleep(100);
            }
        }
    }
}
