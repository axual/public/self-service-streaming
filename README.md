# Self Service Streaming

Examples used in the video series about "Self Service Streaming".

* Part 1: introduction and setting up the stream. [YouTube video](https://www.youtube.com/watch?v=-bT6E8FIMCc) 
* Part 2: setting up the producer application and verify that it is working correctly. [YouTube video](https://www.youtube.com/watch?v=CpTSw4Luafg) 
* Part 3: setting up a consumer application and enable PagerDuty alerting.

In the video, a streaming data pipeline is created simulating application incidents happening with a certain severity, produced to Axual.  

![alt text](/images/pipeline.png "Data pipeline")


